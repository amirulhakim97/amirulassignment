package assignment;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem15 {

	//Hello 
	public static void main(String[] args) {
		ArrayList<Product> productList = new ArrayList<Product>();

		int totalPrice = 0;
		double balanceToPay = 0.0;
		
		Scanner refScanner = new Scanner(System.in);
		
		char userChoice = '\0';
		
		do {
			System.out.println("Enter Product ID: ");
			int productID = refScanner.nextInt();

			System.out.println("Product Name: ");
			String productName = refScanner.next();

			System.out.println("Price in SGD: ");
			int price = refScanner.nextInt();

			System.out.println("Quantity: ");
			int quantity = refScanner.nextInt();
			
			productList.add(new Product(productID,productName,price,quantity));

			System.out.println("Wish to continue(Y/N): ");
			userChoice = refScanner.next().charAt(0);
			
		} while (userChoice=='Y' || userChoice == 'y');
		
		System.out.println("Product ID     Product Name     Price     Quantity");
		
		int sum = 0;
		
		for (Product p: productList) {
			
			p.displayProducts();
			p.getTotalPrice();
			sum+=p.getTotalPrice();
			
		}
		
		System.out.println(sum);
		
		refScanner.close();

		
		
		
		
		// productList.add

//		Product apple = new Product(101,"Apple",700,1);
//		Product apple2 = new Product(101,"Apple",700,1);
//
//		Product samsung = new Product(102,"Samsung",750,2);
//		
//		System.out.println(apple.hashCode());
//		System.out.println(apple2.hashCode());	//hashCode to check if its the same memory address, if same, then check the value using .equals()
//												//if hashCode same, means we're comparing relevant objects
//		System.out.println(apple.equals(apple2));

	}
}
