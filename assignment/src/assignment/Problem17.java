package assignment;

import java.math.BigInteger;

public class Problem17 {

	
	public static void main(String[] args) {
		
		//Method 1 (for loop)
		/*int factorial = 1;
		int number = 8;
		for (int i = number; i >= 1; i--) {
			factorial*=i;
		}
		System.out.println("Factorial of " + number + " is " + factorial);*/
		
		//Method 2 (while loop)
		int number = 6;
		int factorial = 1;
		while(number>=1) {
			factorial*=number;
			
			number--;
		}
		
		System.out.println("Factorial of " + number + " is " + factorial);
		
		//BigInteger
		BigInteger f = new BigInteger("1");  
		for (int i = 0; i < args.length; i++) {
			
		}
		
	}

}
