package assignment;

//How to find common elements in three sorted array?

//Examples:
//input1 = {1, 5, 10, 20, 40, 80} 
//input2 = {6, 7, 20, 80, 100} 
//input3 = {3, 4, 15, 20, 30, 70, 80, 120} 
//Output: 20, 80

public class Problem10 {

	public static void main(String[] args) {

		int input1[] = { 1, 5, 10, 20, 40, 80, 50 };
		int input2[] = { 6, 7, 20, 80, 100, 50 };
		int input3[] = { 3, 4, 15, 20, 30, 70, 80, 120, 50 };

		for (int i = 0; i < input1.length; i++) {
			for (int j = 0; j < input2.length; j++) {

				if (input1[i] == input2[j]) {
					// System.out.println("first" + input1[i]);

					for (int j2 = 0; j2 < input3.length; j2++) {

						if (input2[j] == input3[j2]) {
							System.out.println(input2[j]);
						}
					}

				}
			}
		}

	}

}
