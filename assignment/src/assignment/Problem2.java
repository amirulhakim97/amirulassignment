package assignment;

public class Problem2 {

	public static void main(String[] args) {
		
		System.out.println("---Problem 1---");

		
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j<=i; j++) {		
				System.out.print("* ");

			}
			System.out.println();

		}

		System.out.println("---Problem 2---");

		
		for (int i = 1; i <= 5; i++) { // row 
			int number = 1;

			for (int j = 1; j <= i; j++) { // column	i represents the number of time to print
				//System.out.print("* ");
				System.out.print(number);
				number++;
			}
			System.out.println();

		}
		
		System.out.println("---Problem 3---");
		
		char a = 'A';
		for (int i = 1; i <= 5; i++) {
			
			
			for (int j = 1; j<=i; j++) {
				a++;
				System.out.println(a);
			}
			System.out.println();
			
		}
		
		 
		System.out.println("---Problem 4---");
		
		for (int i = 1; i <= 5; i++) {
			for (int j = 5; i <= j; j--) {		//1 <= 5  ; 5-- to become 1
				System.out.print("* ");

			}
			System.out.println();

		}
		
		System.out.println("---Problem 5---");

		for (int i = 1; i <= 5; i++) {
			int number = 1;

			for (int j = 5; i <= j; j--) {		//1 <= 5  ; 5-- to become 1
				System.out.print(number);
				number++;
			}
			System.out.println();

		}
		
		
		
		
	}

}
