package assignment;

public class Problem20 {

	public static void main(String[] args) {
		//Method 1
		System.out.println("---Method 1---");
		String newStr = "";
		String newStr2 = "";
		String str = "hello";
		
		for (int i = str.length()-1; i >=0 ; i--) {
			newStr += str.charAt(i);					//first for loop checks from the back
		}

		for (int i = 0; i < str.length(); i++) {
			newStr2 += str.charAt(i);					//second for loop checks from the front

		}
		
		System.out.println("Backwords: " + newStr);
		System.out.println("Normal: " + newStr2);

		
		if (newStr.equals(newStr2)) {
			System.out.println("palindrome");
		}else {
			System.out.println("not palindrome");
		}
		
		
		//Method 2
		System.out.println();
		System.out.println("---Method 2---");
		String result = "";
		String str2 = "racecar";
	    int length = str2.length();
	    int front = 0;
	    int back = length - 1;
	    while (back > front) {
	    	
	        char frontChar = str2.charAt(front++);
	        //System.out.println("forward char = " + forwardChar);
	        char backChar = str2.charAt(back--);
	        //System.out.println("backward char = " + backwardChar);
	        
	        if (frontChar != backChar) {
	        	result = "not palindrome";
	        }else {
	        	result = "palindrome";
	        }
	        
	    }
        System.err.println(result);

		
	}
	
	

}
