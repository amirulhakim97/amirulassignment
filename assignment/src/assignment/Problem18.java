package assignment;

import java.util.Scanner;

public class Problem18 {

	public static void main(String[] args) {

		int n, sum = 0, temp, remainder, digits = 0;
		Scanner refScanner = new Scanner(System.in);
		System.out.println("Input a number to check if it is an Armstrong number: ");
		n = refScanner.nextInt();
		refScanner.close();
		temp = n;
		// count number of digits
		while (temp != 0) {
			digits++;
			temp = temp / 10;
		}
		temp = n;
		while (temp != 0) {
			remainder = temp % 10;
			sum = (int) (sum + Math.pow(remainder, digits));
			temp = temp / 10;
		}
		// System.out.println("n="+n+" sum="+sum+" digits="+digits);
		if (n == sum)
			System.out.println(n + " is an Armstrong number.");
		else
			System.out.println(n + " is not an Armstrong number.");

	}

}
