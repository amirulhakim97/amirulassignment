package assignment;


public class Product {

	int productID;
	String productName;
	int price;
	int quantity;
	int itemPrice;
	int totalPrice;
	
	Product(int productID, String productName, int price, int quantity) {
		this.productID = productID;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}

	public void displayProducts() {

		String output = String.format("%-14d %-16s %-9d %d",productID,productName,price,quantity);
		System.out.println(output);
		
		getAmountToPay();
	}
	
	public int getAmountToPay() {
		
		itemPrice = quantity * price;
		return itemPrice;
		
	}
	
	
	public int getTotalPrice() {
		
		totalPrice += itemPrice;
		System.out.println(totalPrice);
		return totalPrice;
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + price;
//		result = prime * result + productID;
//		result = prime * result + ((productName == null) ? 0 : productName.hashCode());
//		result = prime * result + quantity;
//		return result;
//	}

//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Product other = (Product) obj;
//		if (price != other.price)
//			return false;
//		if (productID != other.productID)
//			return false;
//		if (productName == null) {
//			if (other.productName != null)
//				return false;
//		} else if (!productName.equals(other.productName))
//			return false;
//		if (quantity != other.quantity)
//			return false;
//		return true;
//	}

}
