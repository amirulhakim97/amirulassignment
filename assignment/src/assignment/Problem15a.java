package assignment;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Products {
	  // properties
	  private int productID;
	  private String productName;
	  private int quantity;
	  private double priceInSGD;
	  private double totalPrice;

	  // constructor
	  Products(int productID, String pname, int qty, 
	              double price, double totalPrice) {
	    this.productID=productID;
		this.productName = pname;
	    this.quantity = qty;
	    this.priceInSGD = price;
	    this.totalPrice = totalPrice;
	  }

	  // getter methods
	  public int getProductID() {
		    return productID;
		  }
	  public String getProductName() {
	    return productName;
	  }
	  public int getQuantity() {
	    return quantity;
	  }
	  public double getPriceInSGD() {
	    return priceInSGD;
	  }
	  public double getTotalPrice() {
	    return totalPrice;
	  }

	  // displayFormat
	  public static void displayFormat() {
	    System.out.print(
	        "\nProduct ID    Product Name          Price           Quantity   \n");
	  }

	  // display
	  public void display() {
	    System.out.format("%-13d %-20s  %-15.0f %-12d\n", 
	         productID, productName, priceInSGD,quantity);
	  }
	}

	public class Problem15a {
	  public static void main(String[] args) {
	    
	    // variables
	    String productName = null;
	    int quantity = 0;
	    int productID = 0;
	    double price = 0.0;
	    
	    double totalPrice = 0.0;
	    double overAllPrice = 0.0;
	    char choice = '\0';

	    // create Scanner class object
	    Scanner scan = new Scanner(System.in);

	    List<Products> product = new ArrayList<Products>();

	    do {
	      // read input values
	      System.out.println("Enter product details,");
	      System.out.print("Enter Product ID:  ");
	      productID = Integer.parseInt(scan.nextLine());
	      System.out.print("Product Name: ");
	      productName = scan.nextLine();
	      System.out.print("Price in SGD: ");
	      price = scan.nextDouble();
	      System.out.print("Quantity: ");
	      quantity = scan.nextInt();

	      // calculate total price for that product
	      totalPrice = price * quantity;

	      // calculate overall price
	      overAllPrice += totalPrice;

	      // create Product class object and add it to the list
	      product.add( new Products(
	          productID, productName, quantity, price, totalPrice) );

	      // ask for continue?
	      System.out.print("Want to add more item? (y or n): ");
	      choice = scan.next().charAt(0);

	      // read remaining characters, don't store (no use)
	      scan.nextLine();
	    } while (choice == 'y' || choice == 'Y');

	    // display all product with its properties
	    Products.displayFormat();
	    for (Products p : product) {
	      p.display();
	    }

	    // overall price
	    System.out.println("\nTotal Price       " + (int)overAllPrice);
	    System.out.println("Flat Discount     20%");
	    System.out.println("Discount Amount   "+(int)(overAllPrice*0.2));
	    System.out.println("Amount to Pay     "+(int)(overAllPrice*0.8));
	    // close Scanner
	    scan.close();
	  }

	}