package assignment;

import java.util.Scanner;

public class Problem19 {

	public static void main(String[] args) {
//
//		//for-loop Method
//		String str = "Radar";
//		String reversedStr = "";
//		
//		int num1 = 3553;
//		int reversedInt;
//		
//		int strLength = num1.length();
//		
//		for (int i = strLength-1; i >=0; i--) {
//			reversedStr += str.charAt(i);
//		}
//
//		if (reversedStr.toLowerCase().equals(str.toLowerCase())) {
//			System.out.println("Palindrome");
//		}else {
//			System.out.println("Not Palindrome");
//		}
		
		//while-loop method
		int numToCheck = 3553;
		int reversedNum = 0;
		int remainder;
	    
	    // store the number to numToCheck
	    int originalNum = numToCheck;
	    
	    // get the reverse of numToCheck and store into a new variable
	    while (numToCheck != 0) {
	      remainder = numToCheck % 10;  	//to get the last digit
	      reversedNum = reversedNum * 10 + remainder;	
	      numToCheck /= 10;			
	    }
	    
	    if (originalNum == reversedNum) {
	      System.out.println(originalNum + " is Palindrome.");
	    }
	    else {
	      System.out.println(originalNum + " is not Palindrome.");
	    }
 
		
	}	

}
